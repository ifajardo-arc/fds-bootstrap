const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyPlugin = require("copy-webpack-plugin");
const webpack = require("webpack");

const extractSass = new MiniCssExtractPlugin({
  filename: "[name].min.css",
  chunkFilename: "[id].css"
});

module.exports = {
  entry: {
    "fds-bootstrap": "./src/index.js",
    "fds-bootstrap-accessible": "./src/accessible.js"
  },
  output: {
    filename: "[name].min.js",
    path: path.resolve(__dirname, "dist")
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"]
          }
        }
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader?url=false",
          "sass-loader"
        ]
      }
    ]
  },
  resolve: {
    alias: {
      vue: "vue/dist/vue.js"
    },
    extensions: ["*", ".js", ".jsx"]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    extractSass,
    new CopyPlugin([
      { from: path.resolve(__dirname, "node_modules/flight-dev-kit/dist/scss/partials/design-tokens/color.scss"), to: path.resolve(__dirname, "dist")},
      { from: path.resolve(__dirname, "node_modules/flight-dev-kit/dist/scss/partials/design-tokens/color-accessible.scss"), to: path.resolve(__dirname, "dist")},
      { from: path.resolve(__dirname, "node_modules/flight-dev-kit/dist/scss/partials/design-tokens/elevation.scss"), to: path.resolve(__dirname, "dist")},
      { from: path.resolve(__dirname, "node_modules/flight-dev-kit/dist/scss/partials/design-tokens/radius.scss"), to: path.resolve(__dirname, "dist")},
      { from: path.resolve(__dirname, "node_modules/flight-dev-kit/dist/scss/partials/design-tokens/space.scss"), to: path.resolve(__dirname, "dist")},
      { from: path.resolve(__dirname, "node_modules/flight-dev-kit/dist/scss/partials/design-tokens/typography.scss"), to: path.resolve(__dirname, "dist")},
    ])
  ]
};
